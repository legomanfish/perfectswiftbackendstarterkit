# Starter Kit Perfect Backend Swift

This is a simple starter template for creating backend services with Swift and PerfectLib.

## More Information
For more information on the Perfect project, please visit [perfect.org](http://perfect.org).
